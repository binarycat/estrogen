
local function show(post, cols)
	cols = cols or 80
	io.write(post.url.."\n")
	local body = estrogen.html.plain(post.body)
	-- remove trailing newlines
	body = string.gsub(body, "\n*$", "")
	for _, l in ipairs(estrogen.text.lines(estrogen.text.wrap(body, cols-2))) do
		io.write("  "..l.."\n")
	end
	io.write("\n")
end

local timeline = estrogen.stream.map(
	estrogen.mastodon.get("mastodon.technology", "#forth"),
	function(post)
		if not string.match(post.url, "hackers.town") then
			return post
		end
	end
)

local posts, err = timeline:getPosts()
if err ~= nil then error("failed to get posts: "..err) end
for _, post in ipairs(posts) do
	show(post, tonumber(os.getenv("COLUMNS")))
end

local fennel = require("fennel")
fennel.repl({})
