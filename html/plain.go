package html

import (
	"strings"
	
	"golang.org/x/net/html"
)

// attempt to convert an html string to plaintext
func AsPlainText(s string) (string, error) {
	node, err := html.Parse(strings.NewReader(s))
	if err != nil { return "", err }

	b := new(strings.Builder)
	err = NodeToPlainText(node, b)
	if err != nil { return "", err }
	return b.String(), nil
}

func NodeToPlainText(node *html.Node, b *strings.Builder) error {
	if node == nil { return nil }
	var err error
	suffix := ""
	
	switch node.Type {
	case html.TextNode:
		b.WriteString(node.Data)
	case html.ElementNode:
		// ignore non xhtml nodes
		if node.Namespace != "" { break }
		if node.Data == "br" {
			b.WriteByte('\n')
		} else if node.Data == "p" {
			suffix = "\n\n"
		}
		// TODO: handle <a> somehow
	}

	err = NodeToPlainText(node.FirstChild, b)
	if err != nil { return err }
	b.WriteString(suffix)
	err = NodeToPlainText(node.NextSibling, b)
	if err != nil { return err }


	
	return nil
}
