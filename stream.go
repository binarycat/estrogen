package main

import (
	//"log"
	//"fmt"
	
	"github.com/McKael/madon"
	"github.com/yuin/gopher-lua"
	//"layeh.com/gopher-luar"
)

// assumes only one lua.LState will ever be used
var streamMetatableCache lua.LValue

// stream of posts
type Stream interface {
	GetPosts(*madon.LimitParams) ([]*Post, error)
}

type TimelineStream struct {
	Client *madon.Client
	Name string
}

func (st *TimelineStream) GetPosts(lopt *madon.LimitParams) ([]*Post, error) {
	return statusStream(st.Client.GetTimelines(st.Name, false, false, lopt))
}

func (st *TimelineStream) LuaValue(L *lua.LState) lua.LValue {

	return &lua.LUserData{ Value: st, Metatable: getStreamMetatable(L) }
}

func statusStream(statuses []madon.Status, err error) ([]*Post, error) {
	if err != nil {
		return []*Post{}, err
	}
	ret := make([]*Post, 0, len(statuses))

	// using _, v ... &v here leads to a very subtle bug
	for i := range statuses {
		ret = append(ret, &Post{ Status: &statuses[i] })
	}
	return ret, nil
}

func getMastoTimeline(instance, target string, client *madon.Client) (*TimelineStream, error) {
	var err error
	if client == nil {
		client, err = madon.NewApp("estrogen", "https://git.envs.net/~binarycat/estrogen", []string{}, "", instance)
		if err != nil { return nil, err }
	}
	return &TimelineStream{ Name: target, Client: client }, nil
}

func getStreamMetatable(L *lua.LState) lua.LValue {
	if streamMetatableCache == nil {
		mt := L.NewTable()
		mt.RawSetString("__index", L.NewFunction(func(L *lua.LState) int {
			//st := L.Get(1).(*lua.LUserData).Value.(*TimelineStream)
			//_ = st
			idx := L.Get(2).(lua.LString)
			switch idx {
			case "getPosts":
				L.Replace(2, L.NewFunction(func(L *lua.LState) int {
					st := L.Get(1).(*lua.LUserData).Value.(*TimelineStream)
					ret := new(lua.LTable)
					posts, err := st.GetPosts(nil)
					if err != nil {
						L.SetTop(2)
						L.Replace(1, lua.LNil)
						L.Replace(2, lua.LString(err.Error()))
						return 2
					}
					for i, post := range posts {
						//log.Print("got post ",post.Status.ID)
						ret.RawSetInt(i+1, post.LuaValue(L))
					}
					L.SetTop(1)
					L.Replace(1, ret)
					return 1
				}))
				return 1
			default:
				return 0
			}
		}))
		streamMetatableCache = mt
	}
	return streamMetatableCache
}
