package main

import (
	"log"
	//"fmt"
	
	"github.com/McKael/madon"
	"github.com/yuin/gopher-lua"
	//"layeh.com/gopher-luar"
	"git.envs.net/binarycat/estrogen/html"
)

type Post struct {
	Status *madon.Status
	// only set if the post has been rewritten by a script
	Original *madon.Status
	// if set, pre-expand the post if it has a content warning
	AutoExpand bool
}

func (p *Post) LuaValue(L *lua.LState) lua.LValue {
	mt := new(lua.LTable)
	mt.RawSetString("__index", L.NewFunction(func(L *lua.LState) int {
		userdata := L.Get(1).(*lua.LUserData)
		idx := L.Get(2).(lua.LString)
		status := userdata.Value.(*Post).Status
		//L.SetTop(1)
		switch idx {
		case "body":
			L.Insert(lua.LString(status.Content), 3)
			return 1
		case "subject":
			L.Insert(lua.LString(status.SpoilerText), 3)
			return 1
		case "url":
			L.Insert(lua.LString(status.URL), 3)
			return 1
		case "mediatype":
			L.Insert(lua.LString("text/html"), 3)
			return 1
		default:
			return 0
		}
	}))
	return &lua.LUserData{ Value: p, Metatable: mt }
}

const bootString = `
local dir = os.getenv("ESTROGEN_DIR")
package.path = dir.."/lib/?.lua;"..package.path
--package.loaded["fennel"] = dofile(dir.."/base/fennel.lua")
local fennel = require("fennel")
fennel.dofile(dir.."/base/init.fnl")
`

func main() {
	var err error
	
	L := lua.NewState()
	defer L.Close()
	estrogenTbl := L.NewTable()
	mastodonTbl := L.NewTable()
	streamTbl := L.NewTable()
	htmlTbl := L.NewTable()
	mastodonTbl.RawSetString("get", L.NewFunction(func(L *lua.LState) int {
		instance := string(L.Get(1).(lua.LString))
		target, ok := L.Get(2).(lua.LString)
		if !ok {
			target = lua.LString("public")
		}
		
		L.SetTop(1)
		st, err := getMastoTimeline(instance, string(target), nil)
		if err != nil {
			panic(err)
		}
		L.Replace(1, st.LuaValue(L))
		return 1
	}))
	htmlTbl.RawSetString("plain", L.NewFunction(func(L *lua.LState) int {
		s := string(L.Get(1).(lua.LString))
		L.SetTop(2)
		r, err := html.AsPlainText(s)
		if err != nil {
			L.Replace(1, lua.LNil)
			L.Replace(2, lua.LString(err.Error()))
		} else {
			L.Replace(1, lua.LString(r))
			L.Replace(2, lua.LNil)
		}
		return 2
	}))

	estrogenTbl.RawSetString("stream", streamTbl)
	estrogenTbl.RawSetString("html", htmlTbl)
	estrogenTbl.RawSetString("mastodon", mastodonTbl)
	L.SetGlobal("estrogen", estrogenTbl)
	err = L.DoString(bootString)
	if err != nil { log.Fatal("error loading init script: ", err) }


	err = L.DoFile("example.lua")
	if err != nil { log.Fatal("error loading user script: ", err) }
	
}
