(global estrogen _G.estrogen)

(local map_getPosts (fn [st opt]
                      (icollect [_ post (ipairs (st.source:getPosts opt))]
                        (st.func post))))
                           

(fn estrogen.stream.map [st f]
  "create a stream that maps the function `f` over the elements of `st`.
nil returns are elided."

  {:stream_type :map
   :source st
   :func f
   :getPosts map_getPosts})

(set _G.estrogen.text (or estrogen.text {}))


(fn estrogen.text.assureNewline [l]
  "append a newline, if it's not there already."
  (if (not= "\n" (string.sub l -2))
      (.. l "\n")
      l))
(fn estrogen.text.lines [s]
  "split a string on newlines"
  (if (= (type s) "table")
      s
      (icollect [l (string.gmatch s "([^\n]*)\n?")]
        l)))

(fn estrogen.text.wrap [s cols]
  "wrap a string to fit in `cols` columns"
  ;; if the string fits already, return it unmodified
  (if (>= cols (length s))
      s
      (do      
        (var l (string.match s "^([^\n]*\n?)"))
        (if (< cols (length l))
            (do
              ;; truncate line to `cols`
              (set l (string.sub s 1 cols))
              ;; if there's a (partial) word at the end of the line, remove it
              (set l (or (string.match l "^(.+%s+)[^ ]+$") l))))
        ;; output current line, recurse on rest of input
        (.. (estrogen.text.assureNewline l)
            (estrogen.text.wrap (string.sub s (+ 1 (length l))) cols)))))
